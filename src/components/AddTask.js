import React from "react";

class AddTask extends React.Component {
  state = {
    newTask: ""
  };

  handleChange = event => {
    // on stocke la valeur de l'input dans une constante
    const value = event.target.value;
    // on attribue cette valeur dans le state
    this.setState({
      newTask: value
    });
  };

  handleSubmit = (event) => {
    // on utilise l'event pour empecher le naviqateur de se recharger (comportement par défaut de onSubmit)
    event.preventDefault();
  
    // pour pouvoir envoyer les données au parent (App.js) on transforme la props en fonction avec comme argument la valeur à envoyer et on revide l'input
    this.props.addNewTask(this.state.newTask);
    this.setState({
      newTask: ''
    });
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label htmlFor="addTask"> Ajouter une tache </label>
        <input id="addTask" type="text" value={this.state.newTask} onChange={this.handleChange} />
        <button type="submit"> Ajouter </button>
      </form>
    );
  }
}

export default AddTask;
