import React from "react";

const TaskItem = props => {
  const handleClick = () => {
    props.onRemove(props.task.id);
  };

  return (
    <div>
      <span> {props.task.name} </span>
      <button onClick={handleClick}> Fait </button>
    </div>
  );
};

export default TaskItem;
