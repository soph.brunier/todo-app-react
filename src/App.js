import React, { Component } from "react";
import "./App.css";
import Title from "./components/Title";
import TaskContainer from "./components/TaskContainer";
import TaskItem from "./components/TaskItem";
import AddTask from "./components/AddTask";

class App extends Component {
  state = {
    tasks: [
      { id: 1, name: "Faire un truc" },
      { id: 2, name: "Faire autre chose" },
      { id: 3, name: "Faire plus rien" }
    ]
  };

  handleRemove = id => {
    this.setState({
        tasks: this.state.tasks.filter(task => task.id !== id)
    });
  };

  renderTasks = tasks =>
    tasks.map(task => <TaskItem key={task.id} task={task} onRemove={this.handleRemove} />);


  handleAddNewTask = newTask => {
      // on stocke la valeur envoyée par l'enfant dans une constante
    const newTaskName = newTask;

    // On calcule la longueur de l'array pour pouvoir obtenir l'id du dernier élément
    const tasksLength = this.state.tasks.length;
    // On enregistre l'objet task nouvellement crée dans une constante
    const newTaskObject = {id: tasksLength + 1, name: newTaskName};
    
    // on concatene un nouveau tableau a celui de state (car immuable)
    this.setState({
        tasks: this.state.tasks.concat(newTaskObject)
    })
  };

  render() {
    // console.log(this.state.tasks);
    return (
      <div>
        <AddTask addNewTask={this.handleAddNewTask} />
        <Title title="Hello world" />
        <TaskContainer>
          {" "}
          {this.renderTasks(this.state.tasks)}
        </TaskContainer>{" "}
      </div>
    );
  }
}

export default App;
